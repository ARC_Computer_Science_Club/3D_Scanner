#include <iostream>
#include <fstream>
#include <iomanip>
#include <stdlib.h>
#include <limits>

using namespace std;

/// Prototypes
void gcode_out(fstream&, string);
void gcode_out(fstream&, double=0, double=0, double=0);
void gcode_out(fstream&, char, double);
void gcode_out(fstream&, char, double, char, double);

void gcode_init(fstream&); /// Initialize to Absolute Zero Position

struct cli
{
    int quality = 3;
    int circumference = 500;
    int zrange = 125;
    int focalpoint = 0;
    int speed = 40;
    string outfile = "output.gcode";
};

cli ParseCommandLine(int, char*[]);


int main(int argc, char *argv[])
{
    cli args = ParseCommandLine(argc, argv); //parse command line arguments

    fstream out;

    //open outfile for writing, erase if it already exists.
    out.open(args.outfile.c_str(), ios::out | ios::trunc);
    if(!out.is_open())
    {
        cerr << "failed to open file: " << args.outfile << endl;
        exit(EXIT_FAILURE);
    }

    //calculate distance around the base to move between each picture
    double sect = args.circumference / (args.quality * 2);

    //initialize gcode file
    gcode_init(out);

    //iterate over the number of levels
    for(int j = 0; j < args.quality; j++)
    {
        out << endl << "; Level " << j + 1 << endl; //output comment indicating start of new level.

        if(j > 0)
        { //dont execute for first run
            gcode_out(out, 'z', args.zrange / args.quality); //move z axis updward a level
        }

        for(int i = 0; i < args.circumference; i += sect)
        { //turn table sect distance then take picture
            gcode_out(out, "M106 S255 ; Fan On"); //Take photo, Turns fan on to full power
            gcode_out(out, "M106 S0 ; Fan Off"); //Turn fan off
            gcode_out(out, "G4 P500 ; 500 millisecond delay"); //delay in milliseconds
            gcode_out(out,'x', sect); //rotate table sect millimeters
        }
    }
    out.close();
    return 0;
}

void gcode_clear(fstream& out, char filename[]) //depreciated
{
    out.close();
    out.open(filename, ios::out | ios::trunc);
    out.close();
    out.open(filename, ios::out | ios::trunc);
}

void displayHelp()
{
    //Help menu displayed to the user
    cerr << "Usage: path_to_executable [flag] [arg]" << endl;
    cerr << "  [-q | --quality] [int value]         Quality level, higher is better but" << endl;
    cerr << "                                       takes longer. Defaults to 3." << endl << endl;
    cerr << "  [-c | --circumference] [int value]   circumference of bed in mm. Defaults to 500" << endl << endl;
    cerr << "  [-z | --zrange] [int value]          height of z axis in mm. Defaults to 125" << endl << endl;
    cerr << "  [-s | --speed] [int value]           speed of movements [Not yet implimented]" << endl << endl;
    cerr << "  [-o | --outfile] [filename]          Output file. Defaults to out.gcode" << endl << endl;
}

int argToInt(string flag, string arg)
{
    //Converts argument to an integer. Displays an error to the user then exits if a problem arises.
    try
    {
        return stoi(arg);
    }
    catch (const invalid_argument& ia)
    {
        cerr << "flag " << flag << ": Invalid argument " << endl;
        displayHelp();
        exit(EXIT_FAILURE);
    }
    catch(...)
    {
        cerr << "flag " << flag << ": Unrecognized value. Program Failure!" << endl;
        displayHelp();
        exit(EXIT_FAILURE);
    }
}

void argEnforceRange(string flag, int value, int min=numeric_limits<int>::min(), int max=numeric_limits<int>::max())
{
    //Enforces an argument to be within a set range. Displays and error then exits if its not.
    if(!(value >= min && value <= max))
    {
        cerr << "flag " << flag << ": Value should be between " << min << " and " << max << "." << endl;
        exit(EXIT_FAILURE);
    }
}

void enforceArguments(string flag, int flag_pos, int size, int qty)
{
    //Enforces a set amount of arguments are available. Displays and error then exits if not.
    if(!(flag_pos + qty < size))
    {
        cerr << "flag " << flag << ": Expecting " << qty << " argument(s)" << endl;
        exit(EXIT_FAILURE);
    }
}


cli ParseCommandLine(int argc, char *argv[])
{
    cli cmd_data;
    string flag;
    string arg;

    for(int i = 1; i < argc; i++)
    {
        flag = string(argv[i]);

        if (flag == "-h" or flag == "--help")
        {
            displayHelp();
            exit(0);
        }
        else if (flag == "-q" or flag == "--quality")
        {
            enforceArguments(flag, i, argc, 1);
            arg = string(argv[i + 1]);
            int value = argToInt(flag, arg);
            argEnforceRange(flag, value, 1);
            cmd_data.quality = value;
            i++;
        }
        else if (flag == "-c" or flag == "--circumference")
        {
            enforceArguments(flag, i, argc, 1);
            arg = string(argv[i + 1]);
            int value = argToInt(flag, arg);
            argEnforceRange(flag, value, 1);
            cmd_data.circumference = value;
            i++;
        }
        else if (flag == "-z" or flag == "--zrange")
        {
            enforceArguments(flag, i, argc, 1);
            arg = string(argv[i + 1]);
            int value = argToInt(flag, arg);
            argEnforceRange(flag, value, 1);
            cmd_data.zrange = value;
            i++;
        }
        else if (flag == "-s" or flag == "speed")
        {
            enforceArguments(flag, i, argc, 1);
            arg = string(argv[i + 1]);
            int value = argToInt(flag, arg);
            argEnforceRange(flag, value, 1, 150);
            cmd_data.speed = value;
            i++;
        }
        else if (flag == "-o" or flag == "--outfile")
        {
            enforceArguments(flag, i, argc, 1);
            arg = string(argv[i + 1]);
            cmd_data.outfile = arg;
            i++;
        }
        else
        {
            cerr << "Unrecognized flag. Program Failure! :'(" << endl;
            displayHelp();
            exit(EXIT_FAILURE);
        }
    }
    return cmd_data;
}


void gcode_init(fstream& out)
{
    //initialization sequence for gcode output
    gcode_out(out, "G90 ; Use absolute coordinates"); // Set Absolute Coordinates
    gcode_out(out, "G28 ; Home all axis"); //home all axis
    gcode_out(out, 'y', 0, 'f', 1200); //move y to 0 and set feedrate to 1200
    gcode_out(out,'z',0); // move z to 0
    gcode_out(out, "G91 ; Set relative coordinates"); // Set Relative Coordinates
}


void gcode_out(fstream& out, double x, double y, double z)
{
    out << setprecision(3) << fixed << "G0 X" << x << " Y" << y << " Z" << z << endl;
}
/// pass 'x','y', or 'z' into char, automatically capitalizes them
void gcode_out(fstream& out, char dimension, double distance)
{
    out << setprecision(3) << fixed << "G0 " << (char)(toupper(dimension)) << distance << endl;
}
void gcode_out(fstream& out, char dim1, double dist1, char dim2, double dist2)
{
    out << setprecision(3) << fixed << "G0 " << (char)(toupper(dim1)) << dist1 << " " << (char)(toupper(dim2)) << dist2 << endl;
}
void gcode_out(fstream& out, string raw)
{
    out << raw << endl;
}
